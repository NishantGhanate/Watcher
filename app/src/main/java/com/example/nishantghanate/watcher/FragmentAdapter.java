package com.example.nishantghanate.watcher;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class FragmentAdapter extends FragmentPagerAdapter {

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:{


                return new LogsTab1();
            }

            case 1:{
                return new ActionTab2();
            }

        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){

            case 0:{return "Motion Logs";}

            case 1:{return "Action";}


        }
        return super.getPageTitle(position);
    }
}
