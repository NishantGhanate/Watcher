package com.example.nishantghanate.watcher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.CardView;
import java.util.ArrayList;

public class LogsTab1adapter  extends RecyclerView.Adapter<LogsTab1adapter.Holderview>{
    Context context;
    ArrayList<String> itemList;
    ArrayList<String> itemListMotionImages;
    byte[] blob;
    Bitmap bmp;

    public LogsTab1adapter(Context context, ArrayList<String> itemList, ArrayList<String> itemListMotionImages) {
        this.context = context;
        this.itemList = itemList;
        this.itemListMotionImages = itemListMotionImages;
    }


    @NonNull
    @Override
    public LogsTab1adapter.Holderview onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout= LayoutInflater.from(parent.getContext()).
                inflate(R.layout.logstab1carview,parent,false);
        return new Holderview(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull LogsTab1adapter.Holderview holder, int position) {
        holder.textViewTimestamp.setText( itemList.get(position));
        blob = Base64.decode(itemListMotionImages.get(position),Base64.DEFAULT);
        bmp = BitmapFactory.decodeByteArray(blob,0,blob.length);
        holder.myImage.setImageBitmap(bmp);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class Holderview extends RecyclerView.ViewHolder {
        TextView textViewTimestamp;
        ImageView myImage;
        CardView cardView;
        public Holderview(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.card);
            textViewTimestamp = itemView.findViewById(R.id.textViewTimestamp);
            myImage = itemView.findViewById(R.id.imageViewMotion);


        }
    }
}

//            byte[] blob = Base64.decode(itemListTimestamp.get(2),Base64.DEFAULT);
//            Log.d(TAG, " Base64  : " + itemList.get(2) );
//            Bitmap bmp = BitmapFactory.decodeByteArray(blob,0,blob.length);
//            myImage.setImageBitmap(bmp);