package com.example.nishantghanate.watcher;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public  class LogsTab1 extends Fragment {
    private static final String TAG = "Tab1 ";
    String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRefTimeStamp = database.getReference("users").child(uid).child("motionLogs");
    DatabaseReference myRefImages = database.getReference("users").child(uid).child("images");
    DatabaseReference fcmRef = database.getReference("users").child(uid).child("fcmToken");
    RecyclerView recyclerView;
    LogsTab1adapter adapter;
    ArrayList<String> logs = new ArrayList<>();
    ArrayList<String> itemListTimestamp  =  new ArrayList<>();
    ArrayList<String> itemListMotionImages  =  new ArrayList<>();
    public String[] list1 = {"Nishant" , "GHanate"};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Read from the database
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        fcmRef.setValue(refreshedToken);
        return inflater.inflate(R.layout.logstab1,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleViewTimestamp);
        recyclerView.hasFixedSize();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext()); // context in Fragment and this in activity class
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new LogsTab1adapter(getContext(),itemListTimestamp,itemListMotionImages);

        // Read from database


        Log.d(TAG, " Value in onView Created: " + logs );
        Log.d(TAG, " onViewCreated launched " );


        myRefImages.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                Log.d(TAG, " DataSnapshot images: " + dataSnapshot.getValue() );
                itemListMotionImages.add( dataSnapshot.getValue().toString());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        myRefTimeStamp.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Log.d(TAG, " DataSnapshot Timestamp: " + dataSnapshot.getValue() );
                itemListTimestamp.add( dataSnapshot.getValue().toString());
                //recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }







}
